import handlers from '../router/forecast/index';

describe('Hacemos los test para el pronostico por ciudad', () => {
  describe('post', () => {
    test('devuelve los datos de locacion, pasandole la ciudad', async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: 1 }),
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        send: jest.fn(),
      };
      const req = {
        params: {
          city: 'Kiev',
        },
      };
      await handlers({ axios }).post(req, res);
      expect(res.status.mock.calls).toEqual([[200]]);
      expect(res.send.mock.calls).toEqual([[1]]);
    });
  });
});
