import handlers from '../router/weather/index';

describe('Hacemos los test para el endpoint de la pantalla principal', () => {
  describe('post', () => {
    it('devuelve los datos de locacion, pasandole la direccion IP del usuario', async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: 1 }),
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        send: jest.fn(),
      };
      const req = {
        body: '181.46.138.4',
      };
      await handlers({ axios }).post(req, res);
      //console.log(res.status.mock.calls)
      expect(res.status.mock.calls).toEqual([[200]]);
      expect(res.send.mock.calls).toEqual([[1]]);
    });
  });
});
