import handlers from '../router/current/index';

describe('Hacemos los test para el endpoint de la pantalla principal', () => {
  describe('get', () => {
    test('devuelve los datos de ubicación city o la ubicación actual según ip-api y el estado del tiempo actual', async () => {
      const axios = {
        get: jest.fn().mockResolvedValue({ data: 1 }),
      };
      const res = {
        status: jest.fn().mockReturnThis(),
        send: jest.fn(),
      };
      const req = {
        params: {
          city: 'Lisboa',
        },
      }
      await handlers({ axios }).get(req, res);
      expect(res.status.mock.calls).toEqual([[200]]);
      expect(res.send.mock.calls).toEqual([[1]]);
      expect(axios.get.mock.calls).toEqual([
        ['http://api.openweathermap.org/data/2.5/weather?q=Lisboa&appid=da8a8fe29efab59ed9f190bf9241b5b6&lang=es&units=metric']
      ]);
    });
  });
});
