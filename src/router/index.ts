import weather from './weather/index';
import current from './current/index';
import forecast from './forecast/index';

export const router = {
  weather,
  current,
  forecast,
};
