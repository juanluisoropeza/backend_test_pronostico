import * as dotenv from 'dotenv';
dotenv.config();

const handlers = ({ axios }: any) => ({
  post: async (req: any, res: any) => {
    try {
      const info = req.body;
      const query = req.params;
      let urlSearch = '';
      if (info != undefined) {
        const infoByIp = await axios.get(`http://ip-api.com/json/${info.ip}`);
        urlSearch = `http://api.openweathermap.org/data/2.5/forecast?lat=${infoByIp.data.lat}&lon=${infoByIp.data.lon}&appid=${process.env.API_KEY_WEATHER}&lang=es&units=metric&cnt=15`;
      }
      if (query.city != undefined) {
        urlSearch = `http://api.openweathermap.org/data/2.5/forecast?q=${query.city}&appid=${process.env.API_KEY_WEATHER}&lang=es&units=metric&cnt=15`;
      }
      const { data } = await axios.get(urlSearch);
      res.status(200).send(data);
    } catch (e) {
      res.status(404).send({
        code: e.response.status,
        message: e.response.statusText,
      });
    }
  },
});

export default handlers;
