import * as dotenv from 'dotenv';
dotenv.config();

const handlers = ({ axios }: any) => ({
  get: async (req: any, res: any) => {
    try {
      const query = req.params;
      const { data } = await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${query.city}&appid=${process.env.API_KEY_WEATHER}&lang=es&units=metric`);
      res.status(200).send(data);
    } catch (e) {
      res.status(404).send({
        code: e.response.status,
        message: e.response.statusText,
      });
    }
  },
});

export default handlers;
