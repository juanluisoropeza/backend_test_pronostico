import * as dotenv from 'dotenv';

dotenv.config();

const handlers = ({ axios }: any) => ({
  post: async (req: any, res: any) => {
    const info = req.body;
    const infoByIp = await axios.get(`http://ip-api.com/json/${info.ip}`);
    const { data } = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?lat=${infoByIp.data.lat}&lon=${infoByIp.data.lon}&appid=${process.env.API_KEY_WEATHER}&lang=es&units=metric`
    );
    res.status(200).send(data);
  },
});

export default handlers;
