import boom from '@hapi/boom';
import { Response, NextFunction } from 'express';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const joi = require('@hapi/joi');

const validate = (data: any, schema: any) => {
  const { error } = joi.object(schema).validate(data);
  return error;
};

export const validationHandler = (schema: any, check = 'body') => {
  return function (req: any, res: Response, next: NextFunction) {
    const error = validate(req[check], schema);
    error ? next(boom.badRequest(error)) : next();
  };
};
