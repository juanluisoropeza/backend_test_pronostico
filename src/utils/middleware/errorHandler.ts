import boom from '@hapi/boom';
import { config } from '../../config';
import { NextFunction, Request, Response } from 'express';

const withErrorStack = (error: any, stack: any) => {
  if (config.dev) {
    return { ...error, stack };
  }
  return error;
};

export const logErrors = (err: Error, req: Request, res: Response, next: NextFunction) => {
  console.log(err);
  next(err);
};

export const wrapErrors = (err: any, req: Request, res: Response, next: NextFunction) => {
  if (!err.isBoom) {
    next(boom.badImplementation(err));
  }
  next(err);
};

// eslint-disable-next-line @typescript-eslint/no-unused-vars
// omito esta linea por q para express el middleware debe tener todos los atributos
export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  const {
    output: { statusCode, payload },
  } = err;
  res.status(statusCode);
  res.json(withErrorStack(payload, err.stack));
};
