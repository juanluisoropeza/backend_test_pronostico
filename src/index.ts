import express from 'express';
import axios from 'axios';
import cors from 'cors';
import bodyParser from 'body-parser';
import { router } from './router/index';
import { validationHandler } from './utils/middleware/validationHandler';

import { notFoundHandler } from './utils/middleware/notFoundHandler';
import { citySchema } from './utils/schema/current';

const app = express();

// middleware
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// inyeccion de dependencias
const weatherHandlers = router.weather({ axios });
const currentHandlers = router.current({ axios });
const forecastHandlers = router.forecast({ axios });

const baseApi = '/v1';
app.get(baseApi, (req, res) => {
  res.send(`hola mundo`);
});
app.post(`${baseApi}/location`, weatherHandlers.post);
app.get(`${baseApi}/current/:city`, currentHandlers.get);
app.post(`${baseApi}/forecast/:city?`, forecastHandlers.post);

// Catch 404 Error
app.use(notFoundHandler);

// Start express server
app.listen(3000, () => {
  console.log(`--------------- BACKEND NODEJS ----------------`);
  console.log(`------- Listening http://localhost:3000 -------`);
});

module.exports = app;
